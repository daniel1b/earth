
package earth;
public class Ponto {
    String nome;
    String OS;
    String observacao;
    String numeroProj;
    String andamento;
    String operacao;
    String imagem;
    String style;
    String lat;
    String lon;
    String atualizacao;
    String PED;
    public Ponto(){}
    public Ponto(String texto){
        int i1 = texto.indexOf("<name>");
        int i2 = texto.indexOf("</name>");
        if(i1>0&&i2>0)
            nome = texto.substring(i1+"<name>".length(),i2);
        
        i1 = texto.indexOf("<styleUrl>");
        i2 = texto.indexOf("</styleUrl>",i1);
        if(i1>0&&i2>0)
            style = texto.substring(i1+"<styleUrl>".length(),i2);
        
        String txt = TXT.textoEntre("<Data name=\"os\">", "</Data>", texto);
        i1 = txt.indexOf("<value>");
        i2 = txt.indexOf("</value>");
        if(i1>0&&i2>0)
            OS = txt.substring(i1+"<value>".length(),i2); 
        
        txt = TXT.textoEntre("<Data name=\"observacoes\">", "</Data>", texto);
        i1 = txt.indexOf("<value>");
        i2 = txt.indexOf("</value>");
        if(i1>0&&i2>0){
            observacao = txt.substring(i1+"<value>".length(),i2);
      }
        
        txt = TXT.textoEntre("<Data name=\"Nprojeto\">", "</Data>", texto);
        i1 = txt.indexOf("<value>");
        i2 = txt.indexOf("</value>");
        if(i1>0&&i2>0)
            numeroProj = txt.substring(i1+"<value>".length(),i2);
        
        txt = TXT.textoEntre("<Data name=\"andamento\">", "</Data>", texto);
        i1 = txt.indexOf("<value>");
        i2 = txt.indexOf("</value>");
        if(i1>0&&i2>0)
            andamento = txt.substring(i1+"<value>".length(),i2);

        txt = TXT.textoEntre("<Data name=\"operacao\">", "</Data>", texto);
        i1 = txt.indexOf("<b>");
        i2 = txt.indexOf("</b>");
        if(i1>0&&i2>0)
            operacao = txt.substring(i1+"<b>".length(),i2);
                
        txt = TXT.textoEntre("<Data name=\"imagem\">", "</Data>", texto);
        i1 = txt.indexOf("<value><![CDATA[<img src=\"");
        i2 = txt.indexOf("\" width = 300 />]]></value>");
        if(i1>0&&i2>0)
            imagem = txt.substring(i1+"<value><![CDATA[<img src=\"".length(),i2);
        
        txt = TXT.textoEntre("<Data name=\"atualizacao\">", "</Data>", texto);
        i1 = txt.indexOf("<value>");
        i2 = txt.indexOf("</value>");
        if(i1>0&&i2>0)
            atualizacao = txt.substring(i1+"<value>".length(),i2);
        txt = TXT.textoEntre("<Data name=\"PED\">", "</Data>", texto);
        i1 = txt.indexOf("<value>");
        i2 = txt.indexOf("</value>");
        if(i1>0&&i2>0)
            PED = txt.substring(i1+"<value>".length(),i2);
        
        i1 = texto.indexOf("<coordinates>");
        i2 = texto.indexOf(",",i1);
        if(i1>0&&i2>0){
            lat = texto.substring(i1+"<coordinates>".length(),i2);
        }
        
        i1 = texto.indexOf("<coordinates>"+lat+",");
        i2 = texto.indexOf(",0</coordinates>",i1);
        if(i1>0&&i2>0)
            lon = texto.substring(i1+("<coordinates>"+lat+",").length(),i2);     
        
        /*
                System.out.println("<=================================================>");
                System.out.println("nome:\t\t" + nome);
                System.out.println("numero OS:\t" + OS);
                System.out.println("numero Proj:\t" + numeroProj);
                System.out.println("obs:\t\t" + observacao);
                System.out.println("andamento:\t" + andamento);
                System.out.println("operacao?:\t" + operacao);
                System.out.println("style:\t\t" + style);
                System.out.println("lat:\t\t" + lat);
                System.out.println("lon:\t\t" + lon);
                System.out.println("imagem:\t\t" + imagem);*/
    }
}
