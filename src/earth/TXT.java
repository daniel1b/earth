
package earth;


import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.channels.FileChannel;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;


public class TXT {
    public static void atualizaLegenda(String arquivo,int Param,int Nparam,int Exec,int Nvisit) throws Exception{		
	try{
//	this code ensures that all the pixels in the image are loaded
            Image temp = new ImageIcon(arquivo).getImage();
//	reate the buffered image
            BufferedImage bufferedImage = new BufferedImage(temp.getWidth(null), temp.getHeight(null), BufferedImage.TYPE_INT_RGB);
//			copy image to buffered image
            Graphics g = bufferedImage.createGraphics();
//			clear background and paint the image
            g.setColor(Color.black);
            g.fillRect(0, 0, temp.getWidth(null), temp.getHeight(null));
            g.drawImage(temp, 0, 0, null);
            g.fillRect(0, 0, temp.getWidth(null), temp.getHeight(null));
            g.setFont(new Font("Arial Regular", Font.PLAIN, 22));  
            
            g.setColor(new Color(34,177,76));
            g.fillRect(1, 1, temp.getWidth(null)-2, 31);
            g.setColor(Color.black);
            g.drawString("Istalado/Parametrizado", 10, 25);
            g.drawString(""+Param, temp.getWidth(null)-50, 25);
            
            g.setColor(new Color(0,162,232));
            g.fillRect(1, 33, temp.getWidth(null)-2, 32);
            g.setColor(Color.black);
            g.drawString("Istalado/Não parametrizado", 10, 58);
            g.drawString(""+Nparam, temp.getWidth(null)-50, 58);
                        
            g.setColor(new Color(255,242,0));
            g.fillRect(1, 66, temp.getWidth(null)-2, 32);
            g.setColor(Color.black);
            g.drawString("Em Execução", 10, 92);
            g.drawString(""+Exec, temp.getWidth(null)-50, 92);
                        
            g.setColor(Color.white);
            g.fillRect(1, 99, temp.getWidth(null)-2, 32);
            g.setColor(Color.black);
            g.drawString("Não visitado", 10, 125);
            g.drawString(""+Nvisit, temp.getWidth(null)-50, 125);
            
	    g.dispose();
            OutputStream out = new FileOutputStream(arquivo);
            ImageIO.write(bufferedImage, "PNG", out);
            }
        catch (Exception e){
			throw e;
            }		
	}
    public static void escrever(String text, String nomeArq)      {    
        boolean erro = false;
        try{
            Writer out = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream(nomeArq), "UTF-8"));
        try {
            out.write(text);
        } finally {
            out.close();
        } 
        }catch(Exception e){
            erro = true;
        }
        if(!erro)
            JOptionPane.showMessageDialog(null, "gravado com Sucesso", "Resultado export", JOptionPane.INFORMATION_MESSAGE);
        else
            JOptionPane.showMessageDialog(null, "Ocorreu um erro", "Erro na exportacao", JOptionPane.ERROR);
    }
       
    public static String getLocalText(){
        String saida = "";
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter fl = new FileNameExtensionFilter("Arquivo kml", "kml");
        chooser.setFileFilter(fl);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
           saida = chooser.getSelectedFile().getPath();
        }
        return saida;
    }
    public static String textoEntre(String a, String b, String text){
        String saida ="vazio";
        int iini;
        int ifin;
        iini = text.indexOf(a);
        ifin = text.indexOf(b,iini);
        iini += a.length();
        if(iini>0&&ifin>0)
            saida = text.substring(iini, ifin);       
        return saida;
    }            
    public static String ler(String nomeArq){
        String saida = "";
        try {
            FileReader arq = new FileReader(nomeArq);
            BufferedReader lerArq = new BufferedReader(arq);
            String linha = lerArq.readLine();
            // lê a primeira 
            // a variável "linha" recebe o valor "null" quando o processo
            // de repetição atingir o final do arquivo texto 
            while (linha != null) {
                    saida += linha + "\n";
                linha = lerArq.readLine();                
            } // lê da segunda até a última linha
            
            arq.close();
        } catch (IOException e) {
            System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
        }
        return saida;
    }
    public static String getLocalImg(){
        String saida = "";
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter fl = new FileNameExtensionFilter("Imagens", "png", "jpeg");
        chooser.setFileFilter(fl);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
           saida = chooser.getSelectedFile().getPath();
        }
        return saida;
    }
        
    public static void copyto(String origem, String destino){
        try{
            File arquivoOrigem = new File(origem); 
            File arquivoDestino = new File(destino); 
            if (arquivoDestino.exists())  
                arquivoDestino.delete();  

            FileChannel sourceChannel = null;  
            FileChannel destinationChannel = null;  

            try {  
                sourceChannel = new FileInputStream(arquivoOrigem).getChannel();  
                destinationChannel = new FileOutputStream(arquivoDestino).getChannel();  
                sourceChannel.transferTo(0, sourceChannel.size(),  
                        destinationChannel);  
            } finally {  
                if (sourceChannel != null && sourceChannel.isOpen())  
                    sourceChannel.close();  
                if (destinationChannel != null && destinationChannel.isOpen())  
                    destinationChannel.close();  
           }  
        }
        catch(Exception e){           
        }         
    }
    public static String textoApos(char caracter, String frase){
        String saida="";
        frase = ' '+frase;
        int comeco =0;
        do{
            comeco = frase.indexOf(caracter,comeco);            
                 frase = frase.substring(comeco+1);
                 //JOptionPane.showMessageDialog(null, comeco + " " + frase.charAt(comeco) + " - " + frase);
        }while((frase.indexOf(caracter))!=-1);

        saida = frase;
        return saida;
    }
    
    public static void criaDiretorio(String novoDiretorio){  
        try {       
             if (!new File(novoDiretorio).exists()) { // Verifica se o diretório existe.   
                 (new File(novoDiretorio)).mkdir();   // Cria o diretório   
             }   
        } catch (Exception ex) {   
             JOptionPane.showMessageDialog(null,"Erro ao criar o diretório" + ex.toString(),"Err",JOptionPane.ERROR_MESSAGE);   
        }  
    } 
    
     public static String getLocalPasta(){
        String saida = "";
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
           saida = chooser.getSelectedFile().getPath();
        }
        return saida;
    }
     
      public static void copyAll(File origem, File destino, boolean overwrite) throws IOException, UnsupportedOperationException {
        if (!destino.exists()) {
            destino.mkdir();
        }
        if (!origem.isDirectory()) {
            throw new UnsupportedOperationException("Origem deve ser um diretório");
        }
        if (!destino.isDirectory()) {
            throw new UnsupportedOperationException("Destino deve ser um diretório");
        }
        File[] files = origem.listFiles();
        for (int i = 0; i < files.length; ++i) {
            if (files[i].isDirectory()) {
                copyAll(files[i], new File(destino + "\\" + files[i].getName()), overwrite);
            } else {
                System.out.println("Copiando arquivo: " + files[i].getName());
                copy(files[i], new File(destino + "\\" + files[i].getName()), overwrite);
            }
        }
    }
      public static void copy(File origem, File destino, boolean overwrite) throws IOException {

        if (destino.exists() && !overwrite) {
            return;
        }

        FileInputStream source = new FileInputStream(origem);
        FileOutputStream destination = new FileOutputStream(destino);

        FileChannel sourceFileChannel = source.getChannel();
        FileChannel destinationFileChannel = destination.getChannel();

        long size = sourceFileChannel.size();
        sourceFileChannel.transferTo(0, size, destinationFileChannel);

    }
    
}
